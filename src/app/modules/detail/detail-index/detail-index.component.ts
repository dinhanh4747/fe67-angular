import { IMovie } from './../../../model/movie';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieDataService } from 'src/app/services/movie-data.service';

@Component({
  selector: 'app-detail-index',
  templateUrl: './detail-index.component.html',
  styleUrls: ['./detail-index.component.scss'],
})
export class DetailIndexComponent implements OnInit {
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _http: HttpClient,
    private _movieService: MovieDataService,
  ) {}

  movieDetail ?: IMovie | null;

  fetchMovieDetail () {
    const movieId =

      this._activatedRoute.snapshot.params.id;

    this._http
    .get(
      'https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim',
      {
        params: {
          MaPhim : movieId,
        },
        headers: {
          tokenCyberSoft:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJGcm9udCBFbmQgNjciLCJIZXRIYW5TdHJpbmciOiIyOS8wMS8yMDIyIiwiSGV0SGFuVGltZSI6IjE2NDM0MTQ0MDAwMDAiLCJuYmYiOjE2MTc1NTU2MDAsImV4cCI6MTY0MzU2MjAwMH0.N1IDGkovxIU1E2CjtI_QtEJksOO3lxZxuIwXABaa45w',
        },
      }
    )
    .subscribe(
      (res : any) => {
        console.log(res);
        //cap nhat du lieu
        this._movieService.setMovieDetail(res.content)
      },
      (err) => {
        console.log(err);
      }
    );
  }

  ngOnInit(): void {
    this.fetchMovieDetail();

    this._movieService.movieDetail.subscribe((val : IMovie | null ) => {
      this.movieDetail = val;
    })
  }
}
