import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeIndexComponent } from './home-index/home-index.component';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MovieItemComponent } from './movie-item/movie-item.component';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    //Lay module gan nhat khai bao o day
    HomeIndexComponent,
    MovieItemComponent,
  ],
  imports: [CommonModule, MatButtonModule, FlexLayoutModule, MatCardModule, HttpClientModule,RouterModule],
  exports: [HomeIndexComponent],
})
export class HomeModule {}

//Muốn hiện component home thì export ở home.modules sau đó import homeModule vào appModule.
// Tương tự với các module khác

// import { MatButtonModule } from '@angular/material/button' de su dung component material-ui

//import { FlexLayoutModule } from '@angular/flex-layout';

//import {MatCardModule} from '@angular/material/card';

// import { HttpClientModule } from '@angular/common/http'; để call api
