import { IMovie } from './../../../model/movie';
import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { MovieDataService } from 'src/app/services/movie-data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home-index',
  templateUrl: './home-index.component.html',
  styleUrls: ['./home-index.component.scss'],
})
export class HomeIndexComponent implements OnInit,AfterViewInit, OnChanges, OnDestroy {
  //dependency injection
  // injection MovieDataService vao home voi ten la  _movieService
  constructor(
    private _movieService: MovieDataService,
    private _http: HttpClient
  ) {}
  //tao bien hung du lieu lay ve duoc tu movieService

  movieList: IMovie[] = [];

  movieListSubscription : Subscription | undefined;

  fetchMovie() : void {
    this._http
      .get(
        'https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?',
        {
          params: {
            maNhom: 'GP01',
          },
          headers: {
            tokenCyberSoft:
              'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJGcm9udCBFbmQgNjciLCJIZXRIYW5TdHJpbmciOiIyOS8wMS8yMDIyIiwiSGV0SGFuVGltZSI6IjE2NDM0MTQ0MDAwMDAiLCJuYmYiOjE2MTc1NTU2MDAsImV4cCI6MTY0MzU2MjAwMH0.N1IDGkovxIU1E2CjtI_QtEJksOO3lxZxuIwXABaa45w',
          },
        }
      )
      .subscribe(
        (res : any) => {
          console.log(res);
          //cap nhat du lieu
          this._movieService.setMovieList(res.content)
        },
        (err) => {
          console.log(err);
        }
      );
  }

  handleDeleteMovie(): void {
    this._movieService.deleteMovie(1288);
  }
//lifecycle : chay lan dau tien , tuong tu voi didMount()
  ngOnInit(): void {
    // call api
    this.fetchMovie();

    this.movieListSubscription = this._movieService.movieList.subscribe((value) => {
      this.movieList = value;
    });

    // console.log("did mount")
    // // PROMISE : AXIOS
    // const promise = new Promise((resolve, rejects) => {
    //   setTimeout(() => {
    //     rejects('error ');
    //   }, 2000);
    // })

    // //front xuwr lis
    // promise.then((res) => {
    //   console.log(res, "data back end")
    // })
    // .catch((err) => {
    //   console.log(err);
    // })

    // //Observable

    // const observable = new Observable((resolver) => {
    //   setTimeout(() => {
    //     // resolver.next('data 1')
    //     //neu that bai
    //     resolver.next("error 1")
    //   },2000)
    // });

    // //front xu li : get()
    // observable.subscribe((res) => {
    //   console.log(res);
    // },
    // (err) => {
    //   console.log(err);
    // });
  }
//lifecycle : render xong moi chay,
  ngAfterViewInit() {
    console.log("view Init")
  }
//lifecycle : chay khi component nhap vao input tu cha va bi thay doi
  ngOnChanges() {

  }
//lifecycle : chay khi component bi huy tuong tu willUnMount (React)
  ngOnDestroy() {
    // ? : optional changing
    this.movieListSubscription?.unsubscribe();
  }
}
